#ifndef UNIFINGER_H
#define UNIFINGER_H

#include <QtTest/QTest>
#include <QSerialPort>
#include <QDebug>
#include <QObject>
#include <QTimer>

#define CODE_START 0x40
#define CODE_END   0x0A
#define CMDLEN     13

#define CMD_SYSTEM_PARAMETER_WRITE 0x01
#define CMD_SYSTEM_PARAMETER_READ  0x03
#define CMD_SCAN_TEMPLATE          0x21
#define CMD_WRITE_ENC_KEY          0x34

#define PARAM_FIRMWARE_VERSION     0x6E
#define PARAM_SERIAL_NUMBER        0x6F
#define PARAM_TEMPLATE_SIZE        0x64
#define PARAM_SEND_SCAN_SUCCESS    0x75
#define PARAM_ENROLL_MODE          0x65
#define PARAM_ENROLLED_FINGER_CNT  0x73
#define PARAM_AVAIL_FINGER_CNT     0x74
#define PARAM_TIMEOUT              0x62
#define PARAM_MATCHING_TIMEOUT     0x88
#define PARAM_FREE_SCAN            0x84
#define PARAM_AUTO_RESP            0x82
#define PARAM_ENCRYPTION_MODE      0x67

#define SCAN_OK       0x61
#define SCAN_SUCCESS  0x62
#define SCAN_FAIL     0x63
#define SCAN_TIMEOUT  0x6C
#define SCAN_TRYAGAIN 0x6B
#define EXIST_FINGER  0x86
#define EXIST_ID      0x6E
#define NOT_FOUND     0x69

#define ENROLL_ONE_TIME        0x30
#define ENROLL_TWO_TIMES_1     0x31
#define ENROLL_TWO_TIMES_2     0x32
#define ENROLL_TWO_TEMPLATES_1 0x41
#define ENROLL_TWO_TEMPLATES_2 0x42

#define SCAN_SUCCESS_NO        0x30
#define SCAN_SUCCESS_SEND      0x31

class Unifinger : public QObject
{
    Q_OBJECT

public:    
    Unifinger();
    int init( QString port );
    int setParameters(bool);
    int scanTemplate();
    int writeEncryptionKey( QByteArray key );
    quint32 getSerial() { return serial_number; }

private:
    union cast_union
    {
        quint32 integer;
        unsigned char byte[4];
    };

    quint32 serial_number;
    char lastError;
    QSerialPort serial;
    QByteArray response;
    QTimer m_timer; // connection timer

    int readSerial();
    int readParameter( unsigned char param );
    int writeParameter( unsigned char param, qint32 value );    
    unsigned char getChecksum( QByteArray data );
    int sendCmd( QByteArray cmd );

private slots:
    void handleReadyRead();
    void handleTimeout();
    void handleError(QSerialPort::SerialPortError error);

signals:
    void scanComplete( QByteArray );
    void scanError( QString );
};

#endif // UNIFINGER_H
