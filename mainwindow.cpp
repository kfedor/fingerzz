#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    loadSettings();
    waiter = new WaiterDialog( this );

    // coz builtin limits not working on some Gtk themes
    ui->comboBox_userlist->setStyleSheet("combobox-popup: 0;");    

    // Test timer
    /*
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timer_event()));
    timer->start(1000);
    */

    connect( &unifinger, SIGNAL(scanComplete(QByteArray)), this, SLOT(scanComplete(QByteArray)) );
    connect( &unifinger, SIGNAL(scanError(QString)), this, SLOT(scanError(QString)) );

    qsrand( QTime::currentTime().msec() );
}

MainWindow::~MainWindow()
{
    delete ui;
    delete waiter;
}

void MainWindow::showPopup( QString warning )
{
    QMessageBox::information( this, "Info", warning, QMessageBox::Ok );
    ui->statusbar->showMessage( warning, 0);
}

bool MainWindow::initScanner( QString port )
{
    QByteArray b_key = getEncKey();
    bool ifkey = false;

    if( b_key.size() == 32 )
        ifkey = true;

    if( !unifinger.init( port ) || !unifinger.setParameters(ifkey) )
        return false;

    if( ifkey )
    {
        if( unifinger.writeEncryptionKey( b_key ) )
        {
            qDebug() << "Encryption key written";
            return true;
        }
        else
        {
            qDebug() << "Error loading encryption key";
            return false;
        }
    }

    serial_number = unifinger.getSerial();
    ui->lineEdit_bio_device_serial->setText( QString::number(serial_number) );

    return true;
}

void MainWindow::scanComplete(QByteArray templ)
{
    qDebug() << templ.size() << "Scan complete: " << templ.toHex();
    waiter->hide();
    ui->lineEdit_pin1->clear();
    ui->lineEdit_pin2->clear();

    // Get all data
    quint32 index = ui->comboBox_userlist->currentIndex();        
    quint32 user_id = users_id[index];
    QString for_hash = QString( "%1:Fedor I.Kryukov" ).arg(pin);

    QCryptographicHash qhash(QCryptographicHash::Sha1);
    QByteArray user_hash = QCryptographicHash::hash( for_hash.toLatin1(), QCryptographicHash::Sha1 );
    //QString hex_hash = QString( user_hash.toHex() );
    //qDebug() << "Registering user_id:" << user_id << "hash:" << hex_hash;

    // network request
    QString url_finger = host + "/v2/fingerprint";
    sendFingerprintRequest( url_finger, user_id, user_hash.toHex(), templ.toHex(), serial_number );
}

void MainWindow::scanError( QString str )
{
    waiter->hide();
    ui->lineEdit_pin1->clear();
    ui->lineEdit_pin2->clear();

    showPopup( str );
}

void MainWindow::setUserList( QJsonArray arr )
{    
    users_id.clear();
    ui->comboBox_userlist->clear();

    for( qint32 i=0; i<arr.size(); i++)
    {
        QJsonObject obj = arr[i].toObject();
        QString str = obj["fio"].toString();
        qint32 id = obj["id"].toInt();
        ui->comboBox_userlist->addItem( str );        
        users_id.push_back( id );
    }

    ui->statusbar->showMessage( "Userlist loaded", 0 );
}

QString MainWindow::getPIN()
{
    QRegExp digit("\\d+");

    QString pin1 = ui->lineEdit_pin1->text();
    QString pin2 = ui->lineEdit_pin2->text();

    if( pin1.length() == 0 )
    {
        showPopup( "PIN empty" );
        pin1.clear();
    }
    else if( pin1 != pin2 )
    {
        showPopup( "PINs mismatch" );
        pin1.clear();
    }
    else if( digit.exactMatch(pin1) == false )
    {
        showPopup( "PIN is not a digit" );
        pin1.clear();
    }

    return pin1;
}

void MainWindow::on_runScanning_clicked()
{        
    pin = getPIN();

    if( pin.length() == 0 )
    {
        return;
    }

    waiter->show();
    ui->statusbar->showMessage( "Gimme your finger", 0 );

    unifinger.scanTemplate();
}

void MainWindow::loadSettings()
{
    QSettings settings("fingerzzz.ini", QSettings::IniFormat);
    settings.beginGroup("admin");

    ui->lineEdit_atmid->setText( settings.value("atm_id").toString() );
    ui->lineEdit_atmkey->setText( settings.value("atm_key").toString() );
    ui->lineEdit_enckey->setText( settings.value("encryption_key").toString() );
    ui->checkBox_encrypt_on->setChecked( settings.value("encryption_on").toBool() );

    ui->ComboBox_server->addItem( "http://test.api.finance.atm.cox.ru:80" );
    ui->ComboBox_server->addItem( "http://api.stm.amargelov.ru" );
    ui->ComboBox_server->setCurrentIndex( settings.value("host_index").toInt() );

    host = ui->ComboBox_server->currentText();

    settings.endGroup();
}

QString MainWindow::getSetting( QString val )
{
    QSettings settings("fingerzzz.ini", QSettings::IniFormat);
    settings.beginGroup("admin");

    return settings.value(val).toString();
}

void MainWindow::saveSettings()
{
    QSettings settings("fingerzzz.ini", QSettings::IniFormat);

    settings.beginGroup("admin");
    settings.setValue("atm_id", ui->lineEdit_atmid->text() );
    settings.setValue("atm_key", ui->lineEdit_atmkey->text() );
    settings.setValue("encryption_key", ui->lineEdit_enckey->text());
    settings.setValue("encryption_on", ui->checkBox_encrypt_on->isChecked() );
    settings.setValue("host_index", ui->ComboBox_server->currentIndex() );

    settings.endGroup();

    // load settings to finger scanner

}

void MainWindow::on_pushButton_saveSettings_clicked()
{
    saveSettings();
    ui->statusbar->showMessage( "Settings saved", 0 );    
    QApplication::quit();
}


int MainWindow::sendUsersRequest( QString str_url )
{
    QUrl url( str_url );

    // create custom temporary event loop on stack
    QEventLoop eventLoop;

    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    // the HTTP request
    QNetworkRequest req( url );
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    qint32 request_id = qrand() % 9999999999;
    QCryptographicHash qhash(QCryptographicHash::Sha1);

    QString to_crypt = QString::number(request_id) + ":" + getSetting("atm_key");
    QByteArray request_hash = QCryptographicHash::hash( to_crypt.toLatin1(), QCryptographicHash::Sha1 );

    QJsonObject json_obj;
    json_obj["request_hash"] = QString(request_hash.toHex());
    json_obj["request_id"] = request_id;
    json_obj["app_id"] = getSetting("atm_id").toInt();

    QJsonDocument doc(json_obj);

    QByteArray data = doc.toJson();
    QNetworkReply *reply = mgr.post( req, data );
    eventLoop.exec(); // blocks stack until "finished()" has been called

    QByteArray byte_reply;

    if (reply->error() == QNetworkReply::NoError) //success
    {
        byte_reply = reply->readAll();
    }
    else
    {
        qDebug() << "Failure" <<reply->errorString();
        return false;
    }

    delete reply;

    //qDebug() << byte_reply;

    QJsonDocument doc_reply = QJsonDocument::fromJson( byte_reply );
    QJsonObject obj = doc_reply.object();
    QJsonObject body = obj["body"].toObject();
    QJsonObject head = obj["head"].toObject();

    qint32 error_code = head["error_code"].toInt();
    if( error_code != 0 )
        return false;

    QJsonArray arr = body["response"].toArray();

    setUserList( arr );

    return true;
}

void MainWindow::on_pushButton_loadusers_clicked()
{    
    QString url_users = host + "/v1/users";
    if( sendUsersRequest( url_users ) )
    {
        ui->statusbar->showMessage( "User list loaded", 0 );        
        ui->comboBox_userlist->setEnabled(true);
    }
    else
    {        
        showPopup("Unable to load user list");
    }

}

void MainWindow::on_comboBox_userlist_activated(const QString &arg1)
{
    QString label = "Press scan to register fingerprint for " + arg1;
    ui->label_registering->setText(label);
    ui->runScanning->setEnabled(true);

    ui->lineEdit_pin1->setEnabled(true);
    ui->lineEdit_pin2->setEnabled(true);
}

int MainWindow::sendFingerprintRequest( QString str_url, quint32 user_id, QString hash, QString templ, quint32 bio_device_serial )
{
    QUrl url( str_url );

    // create custom temporary event loop on stack
    QEventLoop eventLoop;

    // "quit()" the event-loop, when the network request "finished()"
    QNetworkAccessManager mgr;
    QObject::connect(&mgr, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    // the HTTP request
    QNetworkRequest req( url );
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    qint32 request_id = qrand() % 9999999999;
    QCryptographicHash qhash(QCryptographicHash::Sha1);

    QString to_crypt = QString::number(request_id) + ":" + getSetting("atm_key");
    QByteArray request_hash = QCryptographicHash::hash( to_crypt.toLatin1(), QCryptographicHash::Sha1 );

    QJsonObject json_obj;
    QJsonObject request;
    json_obj["request_hash"] = QString( request_hash.toHex() );
    json_obj["request_id"] = request_id;
    json_obj["app_id"] = getSetting("atm_id").toInt();

    request["hash"] = hash;
    request["template"] = templ;
    request["user_id"] = (qint32)user_id;
    request["bio_device_serial"] = QString::number( bio_device_serial );
    json_obj["request"] = request;

    QJsonDocument doc(json_obj);

    QByteArray data = doc.toJson();
    QNetworkReply *reply = mgr.post( req, data );
    eventLoop.exec(); // blocks stack until "finished()" has been called

    QByteArray byte_reply;

    if( reply->error() == QNetworkReply::NoError ) //success
    {
        byte_reply = reply->readAll();
    }
    else
    {
        qDebug() << "Failure:" <<reply->errorString();
        return false;
    }

    delete reply;

   // qDebug() << byte_reply;

    QJsonDocument doc_reply = QJsonDocument::fromJson( byte_reply );
    QJsonObject obj = doc_reply.object();
    QJsonObject body = obj["body"].toObject();
    QJsonObject head = obj["head"].toObject();

    qint32 error_code = head["error_code"].toInt();
    if( error_code != 0 )
        return false;

    error_code = body["error_code"].toInt();
    QString qerror;

    if( error_code != 0 )
    {
        switch( error_code )
        {
            case 1400:
                qerror = "Error connecting to bio server";
            break;

            case 1402:
                qerror = "Template already assigned to somebody";
            break;

            case 1403:
                qerror = "ID already added";
            break;

            case 1404:
                qerror = "Invalid fingerprint template";
            break;

            case 1405:
                qerror = "Invalid ID";
            break;

            case 1202:
                qerror = "Non existent user";
                break;

            default:
                qerror = QString("Undefined error: %1").arg(error_code);
        };

        showPopup( qerror );

        return false;
    }

    showPopup("Register complete");

    return true;
}

QByteArray MainWindow::getEncKey()
{
    Qt::CheckState state = ui->checkBox_encrypt_on->checkState();

    if( state != Qt::Checked )
        return QByteArray(0);

    QString s_key = ui->lineEdit_enckey->text();
    QByteArray b_key = QByteArray::fromHex( s_key.toLatin1() );

    return b_key;
}
