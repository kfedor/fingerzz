#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) : QDialog(parent), ui(new Ui::Dialog)
{
    ui->setupUi(this);       

    win = new MainWindow( this );

    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->comboBox_comport->addItem( info.portName() );
        //qDebug() << info.systemLocation();
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_connect_clicked()
{
    // Connect
    ui->label_status->setText("Connecting to finger scanner");    

    if( !win->initScanner(ui->comboBox_comport->currentText()))
    {
        ui->label_status->setText("Unable to initialize finger scanner");        
        return;
    }

    hide();
    win->show();
}
