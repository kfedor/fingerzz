#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QtSerialPort/QSerialPortInfo>

#include "mainwindow.h"
#include "unifinger.h"

namespace Ui
{
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_pushButton_connect_clicked();

private:
    Ui::Dialog *ui;
    MainWindow *win;
};

#endif // DIALOG_H
