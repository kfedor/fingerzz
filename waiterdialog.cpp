#include "waiterdialog.h"
#include "ui_waiterdialog.h"

WaiterDialog::WaiterDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WaiterDialog)
{
    ui->setupUi(this);

    setModal(true);
    setWindowModality(Qt::WindowModal);

    Qt::WindowFlags flags = windowFlags();
    flags |= Qt::FramelessWindowHint;
    setWindowFlags( flags );
}

WaiterDialog::~WaiterDialog()
{
    delete ui;
}
