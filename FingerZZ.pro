#-------------------------------------------------
#
# Project created by QtCreator 2015-11-15T23:50:35
#
#-------------------------------------------------

QT       += core gui serialport network testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FingerZZ
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    mainwindow.cpp \
    unifinger.cpp \
    waiterdialog.cpp

HEADERS  += dialog.h \
    mainwindow.h \
    unifinger.h \
    waiterdialog.h

FORMS    += dialog.ui \
    mainwindow.ui \
    waiterdialog.ui
