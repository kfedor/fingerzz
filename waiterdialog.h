#ifndef WAITERDIALOG_H
#define WAITERDIALOG_H

#include <QDialog>

namespace Ui {
class WaiterDialog;
}

class WaiterDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WaiterDialog(QWidget *parent = 0);
    ~WaiterDialog();

private:
    Ui::WaiterDialog *ui;
};

#endif // WAITERDIALOG_H
