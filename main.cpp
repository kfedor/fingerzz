#include "dialog.h"
#include <QApplication>
#include <QInputDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;

    bool ok;
    QString text = QInputDialog::getText( NULL, "Pazzwort?", "Pazzwort:", QLineEdit::Password, "", &ok );

    if( ok == false )
        return false;

    if( text != "legend2015" )
        return false;

    w.show();

    return a.exec();
}
