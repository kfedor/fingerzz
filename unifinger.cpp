#include "unifinger.h"

Unifinger::Unifinger()
{
    connect( &serial, SIGNAL( readyRead() ), SLOT( handleReadyRead() ) );
    connect( &serial, SIGNAL( error( QSerialPort::SerialPortError ) ), SLOT( handleError( QSerialPort::SerialPortError ) ) );
    //connect( &m_timer, SIGNAL( timeout()), SLOT(handleTimeout()) );

    //m_timer.start(5000);
}

void Unifinger::handleReadyRead()
{
    QByteArray tmp = serial.readAll();
    //qDebug() << "handler(): Ready read" << tmp.toHex();
    response += tmp;
    //qDebug() << response.toHex();
/*
    if( response.size() >= CMDLEN )
    {
        qDebug() << "timer stop:" << response.toHex();
        m_timer.stop();
    }
*/

    if( response.size() >= 1 && response[1] == CMD_SCAN_TEMPLATE )
    {
        //qDebug() << response.size();
        if( response.size() >= 10 )
        {
            switch( response[10])
            {
                case SCAN_OK:
                {
                    quint16 sz = (quint8)response[6] | (quint8)response[7] << 8;

                    if( response.size() == CMDLEN+sz+1 )
                    {
                        qDebug() << "Scan template complete";
                        QByteArray tmpl = response.mid(13, response.size()-13-1);
                        emit scanComplete( tmpl );
                    }
                    break;
                }

                default:
                {
                    qDebug() << "Some scanning error happened:" << response.toHex();

                    if( response.size() == CMDLEN )
                        emit scanError(QString("Error or timeout or what"));
                }
            }
        }
    }
}

void Unifinger::handleTimeout()
{
    if (response.isEmpty())
    {
        qDebug() << QObject::tr("No data was currently available for reading from port %1").arg(serial.portName());
    } else {
        qDebug() << QObject::tr("Data successfully received from port %1").arg(serial.portName());
        qDebug() << response;
    }
}


void Unifinger::handleError(QSerialPort::SerialPortError serialPortError)
{
    if( serialPortError == QSerialPort::ReadError )
    {
        qDebug() << QObject::tr("An I/O error occurred while reading the data from port %1, error: %2").arg(serial.portName()).arg(serial.errorString());
    }
}

int Unifinger::init( QString port )
{
    serial.setPortName( port );

    if( !serial.open( QIODevice::ReadWrite ) )
        return 0;

    serial.setBaudRate( QSerialPort::Baud115200 );
    serial.setDataBits( QSerialPort::Data8 );
    serial.setParity( QSerialPort::NoParity );
    serial.setStopBits( QSerialPort::OneStop );

    return 1;
}

int Unifinger::setParameters( bool enc_mode )
{
    if( !readSerial() )
        return 0;

    if( !writeParameter( PARAM_SEND_SCAN_SUCCESS, SCAN_SUCCESS_NO ) )
        return 0;

    if( !writeParameter( PARAM_ENROLL_MODE, ENROLL_ONE_TIME ) )
        return 0;

    if( !writeParameter( PARAM_TIMEOUT, 0x3A ) )
        return 0;

    if( !writeParameter( PARAM_MATCHING_TIMEOUT, 0x33 ) )
        return 0;

    if( enc_mode == true )
    {
        if( !writeParameter( PARAM_ENCRYPTION_MODE, 0x31 ) ) // ON
            return 0;
    }
    else
    {
        if( !writeParameter( PARAM_ENCRYPTION_MODE, 0x30 ) ) // OFF
            return 0;
    }

    return 1;
}

int Unifinger::readSerial()
{
    if( !readParameter( PARAM_SERIAL_NUMBER ) )
        return 0;

    cast_union CU;
    CU.byte[0] = response[6];
    CU.byte[1] = response[7];
    CU.byte[2] = response[8];
    CU.byte[3] = response[9];

    serial_number = CU.integer;

    qDebug() << "Serial:" << serial_number;

    return 1;
}

int Unifinger::writeParameter( unsigned char param, qint32 value )
{
    response.clear();

    QByteArray cmd;

    cmd.push_back( CODE_START );
    cmd.push_back( CMD_SYSTEM_PARAMETER_WRITE );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( value );
    cmd.push_back( value >> 8 );
    cmd.push_back( value >> 16 );
    cmd.push_back( value >> 24 );
    cmd.push_back( param ); // FLAG
    cmd.push_back( getChecksum( cmd ) ); // CMDLEN 13
    cmd.push_back( CODE_END );

    if( !sendCmd(cmd ))
        return 0;

    char lastError = response[10];

    if( lastError != SCAN_OK )
        return 0;

    return 1;
}

int Unifinger::readParameter( unsigned char param )
{
    response.clear();

    QByteArray cmd;

    cmd.push_back( CODE_START );
    cmd.push_back( CMD_SYSTEM_PARAMETER_READ );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( param ); // FLAG
    cmd.push_back( getChecksum( cmd ) ); // CMDLEN 13
    cmd.push_back( CODE_END );

    if( !sendCmd( cmd ) )
        return 0;

    char lastError = response[10];

    if( lastError != SCAN_OK )
        return 0;

    return 1;
}

int Unifinger::sendCmd( QByteArray cmd )
{
    serial.write(cmd);

    if( !serial.waitForBytesWritten(1000) )
        return 0;

    while( serial.waitForReadyRead(100) )
    {}

    //qDebug() << "sendCmd() Read complete" << response.toHex();

    return 1;
}

int Unifinger::scanTemplate()
{
    response.clear();
    qDebug() << "Unifinger::scanTemplate() started";

    QByteArray cmd;

    cmd.push_back( CODE_START );
    cmd.push_back( CMD_SCAN_TEMPLATE );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' ); // FLAG
    cmd.push_back( getChecksum( cmd ) ); // CMDLEN 13
    cmd.push_back( CODE_END );

    serial.write(cmd);

    if( !serial.waitForBytesWritten(1000) )
        return 0;

    //response = serial.readAll();

    /*

    try
    {
        if( !sendCmd( cmd ) )
            throw( "Unifinger::scanTemplate() - sendCmd() fail" );

        lastError = response[10];

        if( lastError != SCAN_OK )
            throw( "Unifinger::scanTemplate() - pre-read fail" );

        int templateSize = response[6] | response[7] << 8;
        qDebug() << "Unifinger::scanTemplate() - template size [%d]\n", templateSize;

        //if( !readReply_vector( templateSize+1 ) )
          //  throw( "Unifinger::scanTemplate() - Read template fail" );

        // Check template
        if( response[response.size()-1] != CODE_END )
            throw( "Unifinger::scanTemplate() - Check template fail" );
    }

    catch( const char *warn )
    {
        qDebug() << warn;
        lastError = SCAN_FAIL;
        return 0;
    }
    */

    lastError = SCAN_OK;
    //lastTemplate = reply_vector;
    //lastTemplate.pop_back(); // remove trailing 0x0A

    return 1;
}

unsigned char Unifinger::getChecksum( QByteArray data )
{
    unsigned char checksum = 0;

    for( qint32 i=0; i<data.size(); i++ )
    {
        checksum += data[i];
    }

    return checksum;
}

int Unifinger::writeEncryptionKey( QByteArray key )
{
    if( key.size() != 32 )
        return 0;

    QByteArray cmd;

    cmd.push_back( CODE_START );
    cmd.push_back( CMD_WRITE_ENC_KEY );
    cmd.push_back( '\0' ); // param
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' ); // size
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' );
    cmd.push_back( '\0' ); // FLAG
    cmd.push_back( getChecksum( cmd ) ); // CMDLEN 13
    cmd.push_back( CODE_END );

    cmd.append( key );
    cmd.push_back( CODE_END );

    //logger.printHex( cmd );

    if( !sendCmd( cmd ) )
        return 0;

    lastError = response[10];

    if( lastError != SCAN_OK )
        return 0;

    return 1;
}
