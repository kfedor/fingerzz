#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QtTest/QTest>
#include <QTimer>
#include <QSettings>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QCryptographicHash>
#include <QMessageBox>
#include <QRegExp>

#include "unifinger.h"
#include "waiterdialog.h"

//#define URL_USERS "http://test.api.finance.atm.cox.ru:80/v1/users"
//#define URL_FINGER "http://test.api.finance.atm.cox.ru:80/v2/fingerprint"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool initScanner( QString port );    

private:
    QVector<quint32> users_id;
    QString pin;
    quint32 serial_number;
    QString host;
    Unifinger unifinger;


    Ui::MainWindow *ui;    
    WaiterDialog *waiter;
    QByteArray getEncKey();
    int sendUsersRequest( QString str_url );
    int sendFingerprintRequest( QString str_url, quint32 user_id, QString hash, QString templ, quint32 bio_device_id );
    void setUserList( QJsonArray arr );
    void loadSettings();
    void saveSettings();    
    QString getSetting( QString val );
    QString getPIN();
    void showPopup( QString warning );

public slots:
    //void timer_event() { qDebug() << "Timer happened"; };

private slots:
    void scanComplete(QByteArray);
    void scanError(QString);

    void on_runScanning_clicked();
    void on_pushButton_saveSettings_clicked();
    void on_pushButton_loadusers_clicked();
    void on_comboBox_userlist_activated(const QString &arg1);
};

#endif // MAINWINDOW_H
